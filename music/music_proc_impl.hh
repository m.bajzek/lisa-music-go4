#ifndef __GO4_MUSIC_PROC_IMPL_HH__
#define __GO4_MUSIC_PROC_IMPL_HH__

#include "music_common.hh"
#include "setup/setup.C"

/* Expand this in Proc ctor */
#define SET_MUSIC_PARAM_PTR \
	fMUSICPar = setup_music();

#define GET_BITS(val,lo,hi)  \
	({ \
		u32 mask = (u32)(1ull << (hi-lo+1)) - 1; \
		(val >> lo) & mask; \
	})
	

/* PTR_HANDLE must point to a start of the FRS MVLC module block, starting with 0xf520, 0xf580, 0xf500 */
#define IS_BARRIER(VAL) \
	( ((VAL >> 16) == 0xf520) || ((VAL >> 16) == 0xf520) || ((VAL >> 16) == 0xf520) )

#define SKIP_FRS_MODULE(DATA_PTR_HANDLE) \
	if(! IS_BARRIER( (*DATA_PTR_HANDLE) )) { \
		fprintf(stderr, "SKIP_FRS_MODULE called, but data pointer isn't on MVLC barrier. It points to: 0x%08x\n", *DATA_PTR_HANDLE); \
	} \
	else { \
		DATA_PTR_HANDLE += ((*DATA_PTR_HANDLE) & 0xffff) + 1; \
	}

/* Chaotic evil alignment. */
#define __EMPTY()
#define __DEREF(x) x __EMPTY()
#define __EVAL(...) __VA_ARGS__

#define MATCH_SUBEV(NAME, SUBEV_HANDLE) \
	SUBEV_HANDLE->GetProcid()  == NAME##_PROCID  && \
	SUBEV_HANDLE->GetControl() == NAME##_CONTROL && \
	SUBEV_HANDLE->GetType()    == NAME##_TYPE    && \
	SUBEV_HANDLE->GetSubtype() == NAME##_SUBTYPE 

#define __UNPACK_FRS_MUSIC_IMPL__ \
	void __PROC_CLASS::UnpackFRSMusic(__EVENT_CLASS* fOut, TGo4MbsSubEvent* psubevt) { \
		u32* pdata = (u32*)psubevt->GetDataField(); \
		u32 lenMax = (psubevt->GetDlen()-2)/2; \
		\
		/* First is the CAEN V830 module, skip it. */ \
		SKIP_FRS_MODULE(pdata) \
		\
		/* Next are 2x V775 MTDC, 2x V785 ADC 
		 * TUM MUSIC's are in geo=10 V785 */  \
		for(int __ii = 0; __ii <= 4; ++__ii) { \
			/* Check if we're correctly on the barrier */ \
			if( !IS_BARRIER((*pdata)) ) { \
				std::cerr << __PRETTY_FUNCTION__ << " ; not on the barrier!\n"; \
				fprintf(stderr, "Word is 0x%08x\n", *pdata); \
				return; \
			} \
			++pdata; \
			if(MUSIC_VME_GEO != GET_BITS((*pdata),27,31)) { --pdata; SKIP_FRS_MODULE(pdata) ; continue; } \
			\
			bool is_good_type = (GET_BITS((*pdata),24,26) == 2); \
			if(!is_good_type) { --pdata; SKIP_FRS_MODULE(pdata) ; continue; } \
			\
			u32 data_words = GET_BITS((*pdata),8,13); \
			++pdata; \
			for(int i=0; i<data_words; ++i) { \
				/* vme_chn is in [0,31], even though only [0,15] are used for MUSIC */ \
				int vme_chn = GET_BITS((*pdata),16,20); \
				fOut->music_e[vme_chn] = GET_BITS((*pdata),0,11); \
				++pdata; \
			} \
			fOut->has_music_data = true; \
			break; \
		} \
	}

/* MUSIC signals also go into MTDC V1290, could be interesting to unpack ... */
#define __UNPACK_FRS_TIMESTAMP_IMPL__ \
	void __PROC_CLASS::UnpackFRSTimestamp(__EVENT_CLASS* fOut, TGo4MbsSubEvent* psubevt) { \
		u32* pdata = (u32*)psubevt->GetDataField(); \
		u32 lenMax = (psubevt->GetDlen()-2)/2; \
		if(lenMax < 5) {std:cerr << __PRETTY_FUNCTION__ << " ; WR subevt has less than 5 words??\n"; return;} \
		/* Unpack next 4 words. They are the timestamp. */ \
		if((*pdata & 0x0000ffff) != FRS_WR_ID) { \
			fprintf(stderr, "Begining of FRS Main Crate, but WR ID doesn't match. Word is 0x%08x\n", *pdata); \
			return; \
		} \
		union { \
			u16 word[4]; \
			u64 wr; \
		} wr; \
		for(u32 i=0; i<4; ++i) wr.word[i] = (u16) *pdata++; \
		fOut->frs_wr = wr.wr; \
	}

#define __MUSIC_CALIB_GET_CHN_FIRED(n) \
	for(int i=0; i<8; ++i) { \
		music##n## _chn_fired[i] = (music_e##n [i] >= 10 && music_e##n [i] < fMUSICPar->max_adc_music##n - 10); \
		music##n## _chn_count++; \
	}

#define MUSIC_COEF(EVENT_PTR, n,I) \
	(( music_e##n [I] * fMUSICPar->e##n##_gain[I] + fMUSICPar->e##n##_off[I] )) \

#define __MUSIC_CALIB_GET_DE(EVENT_PTR, n) \
	if(music##n## _chn_count == 8) { \
		float r1 = MUSIC_COEF(EVENT_PTR, n,1) + MUSIC_COEF(fOut, n,1); \
		float r2 = MUSIC_COEF(EVENT_PTR, n,2) + MUSIC_COEF(fOut, n,3); \
		float r3 = MUSIC_COEF(EVENT_PTR, n,4) + MUSIC_COEF(fOut, n,5); \
		float r4 = MUSIC_COEF(EVENT_PTR, n,6) + MUSIC_COEF(fOut, n,6); \
		de##n = (r1+r2+r3+r4) / 8; \
		music##n##_good_de = 1; \
	} \
	else if(music##n## _chn_count >= 4) { \
		float temp_de = 0.0; \
		for(int i = 0; i<8; ++i) { \
			if(music##n## _chn_fired[i]) temp_de += MUSIC_COEF(fOut, n,i); \
		} \
		de##n = temp_de / music##n## _chn_count; \
		music##n##_good_de = 1; \
	} \
	EVENT_PTR->de_music##n = de##n ; \
	EVENT_PTR->de_music##n##_is_calculated = music##n##_good_de; \
	

#define __CALIB_FRS_MUSIC_IMPL__ \
	void __PROC_CLASS::CalibFRSMusic(__EVENT_CLASS* fOut) { \
		u32* music_e1 = &fOut->music_e[0]; \
		u32* music_e2 = &fOut->music_e[8]; \
		\
		bool music1_chn_fired[8] = {0}; \
		bool music2_chn_fired[8] = {0}; \
		u32 music1_chn_count = 0; \
		u32 music2_chn_count = 0; \
		bool music1_good_de = 0; \
		bool music2_good_de = 0; \
		double de1, de2; \
		\
		__MUSIC_CALIB_GET_CHN_FIRED(1) \
		__MUSIC_CALIB_GET_CHN_FIRED(2) \
		\
		__MUSIC_CALIB_GET_DE(fOut, 1) \
		__MUSIC_CALIB_GET_DE(fOut, 2) \
	}

/* Next directive is for exporting. */
#define UNPACK_MUSIC_IMPL \
		__EVAL (__UNPACK_FRS_MUSIC_IMPL__ ) \
		__EVAL (__UNPACK_FRS_TIMESTAMP_IMPL__ ) \
		__EVAL (__CALIB_FRS_MUSIC_IMPL__ ) \
		\
		void __PROC_CLASS::BuildFRSMusic(TGo4EventElement* output) { \
			auto* fOut = dynamic_cast<__EVENT_CLASS*> (output); \
			auto* fInput = dynamic_cast<TGo4MbsEvent*> (GetInputEvent()); \
			if(nullptr == fInput) { \
				std::cerr << __PRETTY_FUNCTION__ << " InputEvent casted to nullptr.\n"; \
				return; \
			} \
			/* Check for non-physics triggers */ \
			if(fInput->GetTrigger() != 1) return; \
			fInput->ResetIterator(); \
			TGo4MbsSubEvent* psubevt{nullptr}; \
			while((psubevt = fInput->NextSubEvent()) != nullptr) { \
				if( MATCH_SUBEV(MUSIC, psubevt) ) UnpackFRSMusic(fOut, psubevt); \
				else if( MATCH_SUBEV(FRS_MAIN, psubevt) ) UnpackFRSTimestamp(fOut, psubevt); \
			} \
			/*Do the calib/anl next. */ \
			CalibFRSMusic(fOut); \
			/* 
			 * Uh, that's it? Yeah. Deal with it.
			 *
			 *   ################################
			 * ##       ## # ####### ## # #######
			 *           ## # #####   ## # #####
			 *            ## # ###     ## # ###
			 *             ######       ######
			 */ \
		}


#endif
