#ifndef __GO4_MUSIC_COMMON__
#define __GO4_MUSIC_COMMON__

#define MUSIC_DIR music
#define MUSIC_PROCID  30
#define MUSIC_CONTROL 30
#define MUSIC_TYPE    10
#define MUSIC_SUBTYPE  1

#define MUSIC_VME_GEO 10

#define FRS_MAIN_PROCID  10
#define FRS_MAIN_CONTROL 30
#define FRS_MAIN_TYPE	 10
#define FRS_MAIN_SUBTYPE  1
#define FRS_WR_ID 0x100

#define LISA_PROCID  60
#define LISA_CONTROL 20
#define LISA_TYPE    10
#define LISA_SUBTYPE  1

#include <cstdint>
typedef uint64_t u64;
typedef uint32_t u32;
typedef uint16_t u16;
typedef uint8_t   u8;

#endif /* __GO4_MUSIC_COMMON__*/


