#ifndef __GO4_MUSIC_EVENT_IMPL_HH__
#define __GO4_MUSIC_EVENT_IMPL_HH__

#define MUSIC_EVENT_CLEAR \
	memset(music_e, 0, sizeof music_e); \
	has_music_data = 0; \
	de_music1_is_calculated = 0; \
	de_music2_is_calculated = 0; \
	de_music1 = 0; \
	de_music2 = 0; \
	frs_wr = 0; /* TODO: Is this line correct? */ \

#endif
