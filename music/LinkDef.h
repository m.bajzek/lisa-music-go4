#ifdef __CINT__
#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ class TMUSICParameter+; 
#pragma link C++ class TFRSParameter+;
#pragma link C++ class Map1+;
#pragma link C++ class TMWParameter+;
#pragma link C++ class TTPCParameter+;
#pragma link C++ class TMUSICParameter+;
#pragma link C++ class TSCIParameter+;
#pragma link C++ class TIDParameter+;
#pragma link C++ class TLABRParameter+;
#pragma link C++ class TSIParameter+;
#pragma link C++ class TMRTOFMSParameter+;
#pragma link C++ class TRangeParameter+;
#endif


// -*- mode:C++ -*-

#ifdef __CLING__
#pragma link C++ nestedclasses;
#pragma link C++ nestedtypedefs;
#pragma link C++ class TMUSICParameter+; 
#pragma link C++ class TFRSParameter+;
#pragma link C++ class Map1+;
#pragma link C++ class TMWParameter+;
#pragma link C++ class TTPCParameter+;
#pragma link C++ class TMUSICParameter+;
#pragma link C++ class TSCIParameter+;
#pragma link C++ class TIDParameter+;
#pragma link C++ class TLABRParameter+;
#pragma link C++ class TSIParameter+;
#pragma link C++ class TMRTOFMSParameter+;
#pragma link C++ class TRangeParameter+;
#endif
