#include "TProc.h"
#include "../music_proc_impl.hh"

TProc::TProc() {
	SET_MUSIC_PARAM_PTR
}
TProc::~TProc() {}

UNPACK_MUSIC_IMPL(TEvent, TProc)

Bool_t TProc::BuildEvent() {
	BuildFRSMusic(fOut, fIn);
}
