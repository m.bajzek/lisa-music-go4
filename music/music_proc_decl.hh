#ifndef __GO4_MUSIC_PROC_DECL_HH__
#define __GO4_MUSIC_PROC_DECL_HH__

#include "music_common.hh"
#include "TFRSParameter.h" 
#include "TGo4MbsEvent.h"

#define TYPEDEFS(EVENT_CLASS, PROC_CLASS) \
	class EVENT_CLASS; \
	class PROC_CLASS; \
	typedef EVENT_CLASS __EVENT_CLASS; \
	typedef PROC_CLASS __PROC_CLASS;

#define MUSIC_PROC_DECL \
	void UnpackFRSMusic(__EVENT_CLASS*, TGo4MbsSubEvent* ); \
	void UnpackFRSTimestamp(__EVENT_CLASS*, TGo4MbsSubEvent* ); \
	void CalibFRSMusic(__EVENT_CLASS* ); \
	/* Master method */ \
	void BuildFRSMusic(TGo4EventElement* ); \
	\
	std::unique_ptr<TMUSICParameter> fMUSICPar; \
	/* Add histograms by heart */

#endif
