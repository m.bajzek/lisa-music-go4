#ifndef __GO4_MUSIC_EVENT_DECL_HH__
#define __GO4_MUSIC_EVENT_DECL_HH__

#include "music_common.hh"

#define MUSIC_EVENT_DECL \
public: \
	u32 music_e[31]; \
	bool has_music_data; \
	\
	bool de_music1_is_calculated; \
	bool de_music2_is_calculated; \
	double de_music1; \
	double de_music2; \
	\
	u64 frs_wr; \


#endif /* __GO4_MUSIC_EVENT_HH__ */
