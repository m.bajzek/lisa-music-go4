//------------------------------------------------------------------
//        Go4 Release Package v3.02-0 (build 30200) 
//                      21-July-2006
//---------------------------------------------------------------
//	 The GSI Online Offline Object Oriented (Go4) Project
//	 Experiment Data Processing at EE department, GSI
//---------------------------------------------------------------
//
//Copyright (C) 2000- Gesellschaft f. Schwerionenforschung, GSI
//                    Planckstr. 1, 64291 Darmstadt, Germany
//Contact:            http://go4.gsi.de
//----------------------------------------------------------------
//This software can be used under the license agreements as stated
//in Go4License.txt file which is part of the distribution.
//----------------------------------------------------------------
//
// 03-05-17 HS simplified febex4 unpacker for Shizu example code
//          16 E Histograms and 16 Trace "Histograms" 
// 09-05-17 activate reading histograms from autosave file
//          include Shift parameter in Go4 Parameters
// 19-06-17 new data format with timestamps and 4 longwords/ch
// 08-02-18 new data format, only fired channels (zero suppression)
// 09-03-18 set energy gate for some tests
// 12-03-18 calc 2 Energy ratio for coinc tests
// 27-10-18 read energy value from trapezoidal trace (without/with PZ)
// 26-11-18 add 2D histo for some coinc tests
// 10-05-19 new format of energy longword:
//          upper 8bits: sample_time fraction, lower 24bits: energy
// 03-03-20 new format of energy longword: bit31 OV bit, bit30 PU bit
//          (only 6bit sample_time fraction CFD)
// 03-03-20 don't fill OV or PU hits into histograms
// 13-03-20 check sign bit (bit23) in energy value
// 10-06-20 new format of energy longword: 4bit sample_time fraction CFD
// 19-10-20 sample_time increased to 5bit (32 intervalls)
// 01-07-21 fill E histograms also in case of OV, PU
//
//-------------------------------------------------------------------------
#include "TFeb4Event.h"
#include "TFeb4Proc.h"
#include "stdint.h"

#include "Riostream.h"
using namespace std;

#include "TH1.h"
#include "TH2.h"
#include "TCutG.h"
#include "snprintf.h"

#include "TGo4MbsEvent.h"
#include "TGo4WinCond.h"
#include "TGo4PolyCond.h"
#include "TGo4CondArray.h"
#include "TGo4Picture.h"

#include "TFeb4Param.h"
#include "music/music_proc_impl.hh"
UNPACK_MUSIC_IMPL

#define FADC_CHAN 16

#define RAW_DATA_LENGTH 8000
//
#define WR_DATA

#if 0
#define UNPACK_LISA
#endif

//#define CHAN01   5
//#define CHAN02   6

#define CHAN01   4 //11     // 0,1,2...
#define CHAN02   5 //12

//#define SHOW_PILEUP_ONLY 1

#define round(x) ((x)>=0? (int)((x)+0.5):(int)((x)-0.5))

//***********************************************************
TFeb4Proc::TFeb4Proc() : TGo4EventProcessor("Proc")
{
	SET_MUSIC_PARAM_PTR
  cout << "**** TFeb4Proc: Create default instance " << endl;
}
//***********************************************************
TFeb4Proc::~TFeb4Proc()
{
  cout << "**** TFeb4Proc: Delete instance " << endl;
}
//***********************************************************
// this one is used in standard factory
TFeb4Proc::TFeb4Proc(const char* name) : TGo4EventProcessor(name)
{
  cout << "**** TFeb4Proc: Create named instance " << name << endl;

	SET_MUSIC_PARAM_PTR
  Text_t chis[16];
  Text_t chead[64];
  Int_t i;

  //// init user analysis objects:

  // Creation of parameters (check if restored from auto save file):

  if(GetParameter("Par1")==0)
  {// no auto save, create new
      fParam = new TFeb4Param("Par1");
      AddParameter(fParam);
      cout << "**** TFeb4Proc: Created parameter" << endl;
  }
  else // got them from autosave file, restore pointers
  {
      fParam=(TFeb4Param *)GetParameter("Par1");
      cout << "**** TFeb4Proc: Restored parameter from autosave" << endl;
  }

  // Creation of histograms (check if restored from auto save file):
  
  //  if(GetHistogram("Crate1/Cr1Ch01")==0)
  if(GetHistogram("Febex4/Histo01")==0)      // New 09-05-17
  {// no auto save, create new

      for(i=0;i<FADC_CHAN;i++)
      {
          snprintf(chis,15,"Trace_raw%02d",i+1);  
          snprintf(chead,63,"Raw Data channel %2d",i+1);
          fTrace[i] = new TH1I (chis,chead,RAW_DATA_LENGTH,0,RAW_DATA_LENGTH-1);
          AddHistogram(fTrace[i],"Febex4");
      }

      for(i=0;i<FADC_CHAN;i++)
      {
          snprintf(chis,15,"Histo%02d",i+1);  
          snprintf(chead,63,"Histo channel %2d",i+1);
          //fHisto[i] = new TH1I (chis,chead,8192,0,8191);
          fHisto[i] = new TH1I (chis,chead,65536,0,65535);
          AddHistogram(fHisto[i],"Febex4");    
      }

      //fCr1Ch9x10 = new TH2I("HistoCh9x10","E channel 9x10",65536,0,65535,65536,0,65535);    // NEW 26-11-18
      fCr1Ch9x10 = new TH2I("HistoCh9x10","E channel 9x10",1000,0,65535,1000,0,65535); 
      AddHistogram(fCr1Ch9x10);

      for(i=0;i<FADC_CHAN;i++)
      {
          snprintf(chis,15,"Time%02d",i+1);  
          snprintf(chead,63,"Time channel %2d",i+1);
          //fTime[i] = new TH1I (chis,chead,8192,0,8191);
          fTime[i] = new TH1I (chis,chead,8192,-4096,4097);
		  cout << "Added new fTime[ " << i << "]\n";
          AddHistogram(fTime[i],"Febex4");    
      }

#ifdef EXAMPLE_CODE
      for(i =0;i<8;i++)
      {
          snprintf(chis,15,"Cr1Ch%02d",i+1);  snprintf(chead,63,"Crate 1 channel %2d",i+1);
          fCr1Ch[i] = new TH1I (chis,chead,5000,1,5000);
          AddHistogram(fCr1Ch[i],"Crate1");
          snprintf(chis,15,"Cr2Ch%02d",i+1);  snprintf(chead,63, "Crate 2 channel %2d",i+1);
          fCr2Ch[i] = new TH1I (chis,chead,5000,1,5000);
          AddHistogram(fCr2Ch[i],"Crate2");
      }

      fCr1Ch1x2 = new TH2I("Cr1Ch1x2","Crate 1 channel 1x2",200,1,5000,200,1,5000);
      AddHistogram(fCr1Ch1x2);
      fHis1 = new TH1I ("His1","Condition histogram",5000,1,5000);
      AddHistogram(fHis1);
      fHis2 = new TH1I ("His2","Condition histogram",5000,1,5000);
      AddHistogram(fHis2);
      fHis1gate = new TH1I ("His1g","Gated histogram",5000,1,5000);
      AddHistogram(fHis1gate);
      fHis2gate = new TH1I ("His2g","Gated histogram",5000,1,5000);
      AddHistogram(fHis2gate);
      cout << "**** TFeb4Proc: Created histograms" << endl;
#endif
  }
  else // got them from autosave file, restore pointers
  {
      for(i=0;i<FADC_CHAN;i++)   // New 09-05-17
      {
		  cout << "Added autosavd' fTime[ " << i << "]\n";
          snprintf(chis,15,"Trace_raw%02d",i+1); fTrace[i] = (TH1I*)GetHistogram(chis);
          snprintf(chis,15,"Histo%02d",i+1);     fHisto[i] = (TH1I*)GetHistogram(chis);
          snprintf(chis,15,"Time%02d",i+1);      fTime[i] = (TH1I*)GetHistogram(chis);
      } 

      fCr1Ch9x10 = (TH2I*)GetHistogram("HistoCh9x10");   // NEW 26-11-18

#ifdef EXAMPLE_CODE
      for(i =0;i<8;i++)
      {
         snprintf(chis,15,"Crate1/Cr1Ch%02d",i+1); fCr1Ch[i] = (TH1I*)GetHistogram(chis);
         snprintf(chis,15,"Crate2/Cr2Ch%02d",i+1); fCr2Ch[i] = (TH1I*)GetHistogram(chis);
      }

      fCr1Ch1x2 = (TH2I*)GetHistogram("Cr1Ch1x2");
      fHis1     = (TH1I*)GetHistogram("His1");
      fHis2     = (TH1I*)GetHistogram("His2");
      fHis1gate = (TH1I*)GetHistogram("His1g");
      fHis2gate = (TH1I*)GetHistogram("His2g");
#endif

      cout << "**** TFeb4Proc: Restored histograms from autosave" << endl;
  }

#ifdef EXAMPLE_CODE
  // Creation of conditions (check if restored from auto save file):
  if(GetAnalysisCondition("cHis1")==0)
    {// no auto save, create new
      fconHis1= new TGo4WinCond("cHis1");
      fconHis2= new TGo4WinCond("cHis2");
      fconHis1->SetValues(100,2000);
      fconHis2->SetValues(100,2000);
      AddAnalysisCondition(fconHis1);
      AddAnalysisCondition(fconHis2);

      Double_t xvalues[4]={400,700,600,400};
      Double_t yvalues[4]={800,900,1100,800};
      TCutG* mycut= new TCutG("cut1",4,xvalues,yvalues);
      fPolyCon= new TGo4PolyCond("polycon");
      fPolyCon->SetValues(mycut); // copies mycat into fPolyCon
      fPolyCon->Disable(true);   // means: condition check always returns true
      delete mycut; // mycat has been copied into the conditions
      AddAnalysisCondition(fPolyCon);

      xvalues[0]=1000;xvalues[1]=2000;xvalues[2]=1500;xvalues[3]=1000;
      yvalues[0]=1000;yvalues[1]=1000;yvalues[2]=3000;yvalues[3]=1000;
      mycut= new TCutG("cut2",4,xvalues,yvalues);
      fConArr = new TGo4CondArray("polyconar",4,"TGo4PolyCond");
      fConArr->SetValues(mycut);
      fConArr->Disable(true);   // means: condition check always returns true
      delete mycut; // mycat has been copied into the conditions
      AddAnalysisCondition(fConArr);
      cout << "**** TFeb4Proc: Created conditions" << endl;
    }
  else // got them from autosave file, restore pointers
    {
      fPolyCon  = (TGo4PolyCond*) GetAnalysisCondition("polycon");
      fConArr   = (TGo4CondArray*)GetAnalysisCondition("polyconar");
      fconHis1  = (TGo4WinCond*)  GetAnalysisCondition("cHis1");
      fconHis2  = (TGo4WinCond*)  GetAnalysisCondition("cHis2");
      fconHis1->ResetCounts();
      fconHis2->ResetCounts();
      fPolyCon->ResetCounts();
      fConArr->ResetCounts();
      cout << "**** TFeb4Proc: Restored conditions from autosave" << endl;
    }
  // connect histograms to conditions. will be drawn when condition is edited.
  fconHis1->SetHistogram("His1");
  fconHis2->SetHistogram("His2");
  fconHis1->Enable();
  fconHis2->Enable();
  fPolyCon->Enable();
  ((*fConArr)[0])->Enable();
  ((*fConArr)[1])->Enable(); // 2 and 3 remain disabled

  if (GetPicture("Picture")==0)
    {// no auto save, create new
      // in the upper two pads, the condition limits can be set,
      // in the lower two pads, the resulting histograms are shown
      fcondSet = new TGo4Picture("condSet","Set conditions");
      fcondSet->SetLinesDivision(2,2,2);
      fcondSet->LPic(0,0)->AddObject(fHis1);
      fcondSet->LPic(0,1)->AddObject(fHis2);
      fcondSet->LPic(0,0)->AddCondition(fconHis1);
      fcondSet->LPic(0,1)->AddCondition(fconHis2);
      fcondSet->LPic(1,0)->AddObject(fHis1gate);
      fcondSet->LPic(1,1)->AddObject(fHis2gate);
      fcondSet->LPic(1,0)->SetFillAtt(4, 1001); // solid
      fcondSet->LPic(1,0)->SetLineAtt(4,1,1);
      fcondSet->LPic(1,1)->SetFillAtt(9, 1001); // solid
      fcondSet->LPic(1,1)->SetLineAtt(9,1,1);
      AddPicture(fcondSet);

      fPicture = new TGo4Picture("Picture","Picture example");
      fPicture->SetLinesDivision(3, 2,3,1);
      fPicture->LPic(0,0)->AddObject(fCr1Ch[0]);
      fPicture->LPic(0,0)->SetFillAtt(5, 3001); // pattern
      fPicture->LPic(0,0)->SetLineAtt(5,1,1);
      fPicture->LPic(0,1)->AddObject(fCr1Ch[1]);
      fPicture->LPic(0,1)->SetFillAtt(4, 3001); // pattern
      fPicture->LPic(0,1)->SetLineAtt(4,1,1);
      fPicture->LPic(1,0)->AddObject(fCr1Ch[2]);
      fPicture->LPic(1,0)->SetFillAtt(6, 1001); // solid
      fPicture->LPic(1,0)->SetLineAtt(6,1,1);
      fPicture->LPic(1,1)->AddObject(fCr1Ch[3]);
      fPicture->LPic(1,1)->SetFillAtt(7, 1001); // solid
      fPicture->LPic(1,1)->SetLineAtt(7,1,1);
      fPicture->LPic(1,2)->AddObject(fCr1Ch[4]);
      fPicture->LPic(3,0)->AddObject(fCr1Ch1x2);
      fPicture->LPic(3,0)->SetDrawOption("CONT");
      AddPicture(fPicture);
      cout << "**** TFeb4Proc: Created pictures" << endl;
    }
  else  // got them from autosave file, restore pointers
    {
      fPicture = GetPicture("Picture");
      fcondSet = GetPicture("condSet");
      cout << "**** TFeb4Proc: Restored pictures from autosave" << endl;
    }
#endif  // end EXAMPLE_CODE
}
//-----------------------------------------------------------
//
// event function
Bool_t TFeb4Proc::BuildEvent(TGo4EventElement* target)
{  // called by framework. We dont fill any output event here at all

	BuildFRSMusic(target);
  TGo4MbsSubEvent* psubevt;
  //  Int_t index=0;
  //  Float_t value1=0;
  //  Float_t value2=0;
  //  Float_t value=0;

  int i;

  //Int_t lwords;
  //Int_t *pdata;

  UInt_t         l_l;
  Int_t          *pl_se_dat;
  Int_t          *pl_tmp;

  UInt_t          l_dat_len;  
  UInt_t          l_dat_len_byte;  
  
  UInt_t         l_dat;
  UInt_t         l_trig_type;
  //UInt_t         l_trig_type_triva;
  UInt_t         l_sfp_id;
  UInt_t         l_feb_id;
  UInt_t         l_cha_id;
  //UInt_t         l_n_hit;
  //UInt_t         l_hit_id;
  //UInt_t         l_hit_cha_id;
  //Long64_t       ll_time;
  //Long64_t       ll_trg_time;
  //Long64_t       ll_hit_time;
  //UInt_t         l_ch_hitpat   [MAX_SFP][MAX_SLAVE][N_CHA];  
  //UInt_t         l_ch_hitpat_tr[MAX_SFP][MAX_SLAVE][N_CHA];  
  //UInt_t         l_first_trace [MAX_SFP][MAX_SLAVE];

  UInt_t         l_cha_head;  
  UInt_t         l_cha_size;
  UInt_t         l_trace_head;
  UInt_t         l_trace_size;
  UInt_t         l_trace_trail;

  UInt_t         l_spec_head;
  UInt_t         l_spec_trail;
  //UInt_t         l_n_hit_in_cha;
  //UInt_t         l_only_one_hit_in_cha;
  //UInt_t         l_more_than_1_hit_in_cha;  
  //UInt_t         l_e_filt_out_of_trig_wind;
  //UInt_t         l_hit_time_sign;
  // Int_t         l_hit_time;
  //UInt_t         l_hit_cha_id2;
  //UInt_t         l_fpga_energy_sign;

  //UInt_t      l_fpga_energy;
  Int_t       l_fpga_energy;      // signed now!!!
  Int_t       d_t;
  double      d_t2;
  double      q_e;

  UInt_t      l_dat_fir;
  UInt_t      l_dat_sec;

  //Int_t      l_dat_fir;
  //Int_t      l_dat_sec;

  //short       l_dat_fir;
  //short       l_dat_sec;

  //UInt_t      data_hs1;
  //UInt_t      data_hs2;
  UInt_t      header1;
  UInt_t      header2;
  UInt_t      dummy;
  UInt_t      ts_h;
  UInt_t      ts_l;
  UInt_t      time[16];
  UInt_t      time_fract[16];    // NEW 10-05-19 
  UInt_t      e_data[16];        // NEW 08-06-17
  UInt_t      pu_flg[16];
  UInt_t      pattern;
  UInt_t      pileup;

  double e1,e2,sum_e;

  Int_t       baseline,
              max=0;
 
  static int cnt=0;

  //static UInt_t    l_slaves=0;
  //static UInt_t    l_trace=0;
  //static UInt_t    l_e_filt=0;

  Int_t sign_bit=0;      // NEW 13-03-20
  
  Int_t ov_pu_flags=0;   // NEW 03-03-20
  char test_flag=0,      // NEW 28-02-20, test a specific channel, here: 18
       tr_clr_flag=0;    // clear trace histograms once for every event

  Int_t timestamp_l, timestamp_h;
  static int timestamp_old=0, timestamp_new;

  //------------------------------------------------

  fInput  = dynamic_cast<TGo4MbsEvent*>(GetInputEvent());
  if(fInput == 0)
  {
    cout << "AnlProc: no input event !"<< endl;
    return kFALSE;
  }
  auto* fOutput = dynamic_cast<TFeb4Event*>(target);
  if(fOutput == 0)
  {
    cout << "AnlProc: no output event !"<< endl;
    return kFALSE;
  }

#ifdef EXAMPLE_CODE
  if(fInput->GetTrigger() > 11)
  {
      cout << "**** TFeb4Proc: Skip trigger event"<<endl;
      return kFALSE;
  }
#endif

  // first we fill the arrays fCrate1,2 with data from MBS source
  // we have up to two subevents, crate 1 and 2
  // Note that one has to loop over all subevents and select them by
  // crate number:   psubevt->GetSubcrate(),
  // procid:         psubevt->GetProcid(),
  // and/or control: psubevt->GetControl()
  // here we use only crate number

  fInput->ResetIterator();

  while((psubevt = fInput->NextSubEvent()) != nullptr) {
	  if(MATCH_SUBEV(LISA, psubevt)) break;
  }

  pl_se_dat = psubevt->GetDataField();
  l_dat_len = psubevt->GetDlen();
  l_dat_len_byte = (l_dat_len - 2) * 2; 
  //lwords= psubevt->GetIntLen();

  pl_tmp = pl_se_dat;
  
#ifdef UNPACK_LISA
#ifdef WR_DATA
  //pl_tmp = pl_tmp + 5;    // skip white rabbit data
  pl_tmp++;      // skip ID

  timestamp_l = *pl_tmp++;
  timestamp_h = *pl_tmp++;
  //printf("Timestamp_low:  0x%08x\n",timestamp_l);
  //printf("Timestamp_high: 0x%08x\n",timestamp_h);
  timestamp_new = ((timestamp_h&0x0000FFFF)<<16) | timestamp_l&0x0000FFFF;
  //printf("Timestamp_new:  0x%08x\n",timestamp_new);
  //printf("Timestamp_old:  0x%08x\n\n",timestamp_old);
  //printf(" Timestamp_dt:  0x%08x\n\n",timestamp_new-timestamp_old);

  fTime[2]->Fill((int) ((timestamp_new-timestamp_old)/1000.0));

  timestamp_old = timestamp_new;

  pl_tmp++;
  pl_tmp++;	 
#endif
  
  if ( (*pl_tmp) == 0xbad00bad)
  {
    printf ("ERROR>> found bad event (0xbad00bad) \n");
	return kFALSE;
    //goto bad_event;
  }

  //***************************************************
  //sleep(1);  //to see traces when reading from a file

  tr_clr_flag=0;

  //****************************************************
  // loop over one special data block ("sum channel")
  // and 16 trace data blocks
  //****************************************************
 
  while ( (pl_tmp - pl_se_dat) < (l_dat_len_byte/4) )
  {
    l_dat = *pl_tmp++;   // must be padding word or channel header

    if ( (l_dat & 0xfff00000) == 0xadd00000 ) // begin of padding 4 byte words
    {
      //printf ("padding found \n");
      l_dat = (l_dat & 0xff00) >> 8;
      pl_tmp += l_dat - 1;  // increment by pointer by nr. of padding  4byte words 
    }
    else if ( (l_dat & 0xff) == 0x34)  //channel header
    {
      l_cha_head = l_dat;

      l_trig_type = (l_cha_head & 0xf00)      >>  8;
      l_sfp_id    = (l_cha_head & 0xf000)     >> 12;
      l_feb_id    = (l_cha_head & 0xff0000)   >> 16;
      l_cha_id    = (l_cha_head & 0xff000000) >> 24;   // 0xFF: sum chan, 0..15: trace channels

      if ((l_trig_type != 1) && (l_trig_type != 3)) {
          printf("trigger type %d found!\n\n",l_trig_type);
	  fflush(stdout);
      }	
	
      //************************************************************
      // Sum Channel Package, l_cha_id=0xff
      // (Shizu example code: header/trailer IDs are NOT 0xaf/0xbf!)
      //************************************************************

      if ( (l_cha_head & 0xff000000) == 0xff000000) // sum channel 0xff for E,t from fpga 
      {
        //printf("summary channel found!\n\n");
        //fflush(stdout);
        //goto bad_event;
 
        l_cha_size = *pl_tmp++;    // from gosip

	if (l_cha_size > 0x30) {
	  printf("l_cha_size: 0x%08x\n",l_cha_size);   // XXX
	  fflush(stdout);
	  return kTRUE;
	}  
	//test
	//std::cout<<"test!!!!!!!!!!!"<<std::endl;

        //****************************
        // new summary channel data
        // HS 19-06-17  (08-06-17)
        //****************************

        // 4 Header longwords
 
        header1 = *pl_tmp++; 
        header2 = *pl_tmp++; 
        pattern = *pl_tmp++;     // NEW 01-11-17,  New Data Format Header
        dummy = *pl_tmp++;
        //pattern = *pl_tmp++;

        pileup = (pattern>>16)&0x0000FFFF;
        pileup = pileup & pattern;            // NEW 01-11-17, ignore pileup hits in nonEhit channels

	
        //printf("pattern: 0x%08x\n",pattern);

	//*************************************
        // show only pileup events (for test)
        //*************************************
	/*
        if (fParam->show_pu)    // if TRUE show only pileup events
	{
          if (pileup == 0)    // show only pileup events (for test)
	    return kTRUE;
        }
	*/
	
        if (dummy != 0xdeadbeef) 
	{
	    cnt=0;

            printf(" Header  TS  hi: 0x%08x\n",header1);
            printf(" Header  TS low: 0x%08x\n",header2);
            printf(" Header   Dummy: 0x%08x\n",dummy);
            printf(" Header Pattern: 0x%08x\n\n",pattern);
            printf(" Header  Pileup: 0x%08x\n\n",pileup);
            fflush(stdout);
	}

#ifdef SHOW_HEADER
        cnt=cnt+1;
        if (cnt>=500)
	{
          cnt=0;

          printf(" Header  TS  hi: 0x%08x\n",header1);
          printf(" Header  TS low: 0x%08x\n",header2);
          printf(" Header   Dummy: 0x%08x\n",dummy);
          printf(" Header Pattern: 0x%08x\n",pattern);
          
          fflush(stdout);
        }
#endif

        for (i=0;i<16;i++)
	{
	  e_data[i]=0;
          time[i]=0;
	  time_fract[i]=0;    // NEW 10-05-19
	  pu_flg[i]=0;

        }

        //*******************
        // summary chan loop
        //*******************

        for (i=0;i<16;i++)    // NEW 08-06-17
	{
          if (pattern & 1<<i)   // NEW 08-02-18 process data with zero suppression
	  {
              ts_h =  *pl_tmp++;
              ts_l =  *pl_tmp++;
	      
              //l_fpga_energy = *pl_tmp++;
	      
              dummy = *pl_tmp++;                           // contains energy and upper 8bit ZC fraction (test)

              l_fpga_energy = dummy & 0x00FFFFFF;
	      sign_bit = (dummy & 0x00800000)>>23;                   // NEW 13-03-20
	      if (sign_bit) {
                l_fpga_energy = l_fpga_energy | 0xFF000000;
	      }

	      ov_pu_flags = (dummy & 0xC0000000);     // bit31=OV, bit30=PU
	      pu_flg[i] = (dummy & 0x70000000)>>30;
		
              //time_fract[i] = (dummy & 0xFF000000)>>24;   // 8bit
	      time_fract[i] = (dummy & 0x3F000000)>>24;   // 6bit
	      //time_fract[i] = (dummy & 0x3E000000)>>25;   // 5 from 6bit
	      //time_fract[i] = (dummy & 0x0F000000)>>24;     // 4bit
	      //time_fract[i] = (dummy & 0x1F000000)>>24;     // 5bit

	      if(i>2)                           // NEW 08-10-20, fill time fraction except first (two) three channels 
	        fTime[l_feb_id*16+i]->Fill(time_fract[i]);
	      //fTime[l_feb_id*16+i]->Fill((time_fract[i]>>2));   // store 6bit as 4bit

              //----------------------------------------------------------------
	      
              time[i] = ts_l;  

              if (l_fpga_energy<0)    // underflow 
	      {
                //printf("negative Energy: %d, 0x%08x\n",l_fpga_energy,l_fpga_energy);
                //printf("Pileup: 0x%04x\n\n",pileup);
                //printf("huge Energy: 0x%08x\n",l_fpga_energy);   
                fflush(stdout);
                e_data[i] = 0;
              }
              else
	      {
                //return kTRUE;                           // YYY,Test, show only negative energies!!!
                e_data[i] = l_fpga_energy>>fParam->shift;
              }

              //if (e_data[i]>65534)    // overflow
              //  e_data[i]=65534;

              // set condition on energy value, test 09-03-18
              
              //if (i==9)
	      //{
              //  if (e_data[i] < 32600 || e_data[i] > 32900)
              //    return kTRUE;
              //}

              //**************************************************
              // NEW 27-10-18
              // disable here if energy comes from trace data!!!

	      //if (ov_pu_flags == 0 && sign_bit == 0)         // NEW 13-03-20
		  fHisto[l_feb_id*16+i]->Fill(e_data[i]);    // YYY
	      
	      /*
	      if (ov_pu_flags) {
		printf("ov_pu_flags: 0x%08x\n",ov_pu_flags);
		fflush(stdout);
              }		
	      */
	      
              //**************************************************

              if (cnt>=10000) 
	      {
                 cnt=0;
                 printf("Chan%02d, TS high: 0x%08x\n",i,ts_h);
                 printf("Chan%02d, TS  low: 0x%08x\n",i,ts_l);
                 printf("Chan%02d,       E: 0x%08x\n\n",i,l_fpga_energy);
                 
                 fflush(stdout);
              } 

              dummy = *pl_tmp++;
          }
        }   // end summary chan

        //****************************
        // Timestamp and Energy Test
        // split signal and connect  
        // to ch 9 and 10
        //****************************

        //if ((pattern & 0x0600) == 0x600)            // NEW 26-11-18
	//if ((pattern & 0x060) == 0x60)    // ch5,6
	if ((pattern & 0x1800) == 0x1800)      // ch11,12
	{
          if (e_data[CHAN01]!=0 && e_data[CHAN02]!=0)
	    fCr1Ch9x10->Fill(e_data[CHAN01],e_data[CHAN02]);
        }

//#ifdef TS_TEST
        //if ((pattern & 0x0600) == 0x600)      // XXX
	//if ((pattern & 0x060) == 0x060)     // ch5,6
	//if ((pattern & 0x1800) == 0x1800)     // ch11,12
	if ((pattern & 0x0030) == 0x0030)     // ch4,5
	{
          //e1 = (double) 0.047*e_data[CHAN01] - 9.78;
	  //e2 = (double) 0.039*e_data[CHAN02] - 6.95;
	  
          e1 = (double) 0.030*e_data[CHAN01] - 5.32;     // 13-02-21 cluster lab
	  e2 = (double) 0.037*e_data[CHAN02] - 7.28;   
	  
	  sum_e = e1+e2;

	  //printf("E1: %lf\n",e1);
	  //printf("E2: %lf\n\n",e2);

	  //if (sum_e > 1150.0 && sum_e < 1200)
	  //if (e1>1140.0 && e1<1210.0 && e2>1300.0 && e2<1360.0)    // 13-02-21 cluster lab
	  {  
	    //printf("Sum_E: %lf\n",sum_e);
	  
          //*** time check ***

          //d_t = time[CHAN02]-time[CHAN01]+1000;                 // fill around chan 1000...
          //d_t2 = 10.0*((double)time[CHAN02]-(double) time[CHAN01]) + 1000.0;
          d_t = time[CHAN02]-time[CHAN01];                 // new 15-02-21
          d_t2 = 10.0*((double)time[CHAN02]-(double) time[CHAN01]);
	  
          //d_t2 = d_t2 + 10.0*(time_fract[10]/256.0 -time_fract[9]/256.0);      // 8bit
	  d_t2 = d_t2 + 10.0*(time_fract[CHAN02]/64.0 -time_fract[CHAN01]/64.0);        // 6bit
	  //d_t2 = d_t2 + 10.0*(time_fract[10]/32.0 -time_fract[9]/32.0);        // 5bit
	  //d_t2 = d_t2 + 10.0*(time_fract[10]/16.0 -time_fract[9]/16.0);        // 4bit

	  //if (d_t>0 && d_t<8100 && e_data[9]>36150 && e_data[9]<36350 && e_data[10]>32450 && e_data[10]<32650) {   // 60-Co
	  //if (d_t>0 && d_t<8100 && e_data[9]>9700 && e_data[9]<10100 && e_data[10]>34700 && e_data[10]<35100) {   // 133-Ba  
            fTime[0]->Fill(d_t);
	    //if (e_data[9]>25000)
	    //fTime[1]->Fill((int)d_t2);
	    fTime[1]->Fill(round(d_t2));

	    //fTime[9]->Fill(time_fract[9]>>2);      // sample time fraction distribution
	    //fTime[10]->Fill(time_fract[10]>>2);

	    //fTime[9]->Fill(time_fract[9]);           // sample time fraction distribution
	    //fTime[10]->Fill(time_fract[10]);
	    
	  //}  
          //else 
	  //{
          //  fTime[0]->Fill(500);
	  //  fTime[1]->Fill(500);
          //}
	    
          //printf("fTime[0]: %d\n\n",1000+time[10]-time[9]);
          }
	  
          //*** energy check ***

 #ifdef QE_CHECK
	  if (e_data[CHAN02] > 0)
            q_e = 1000.0*((double)e_data[9]/(double)e_data[CHAN02]);   // NEW 12-03-18
          else
	    q_e = 0.0;

          if(q_e>940.0 && q_e<1100.0)   // NEW 28-11-18, show only traces of 'strange' events
            return kTRUE;
	  else {
	    printf("\n");
	    printf("   pattern: 0x%08x\n",pattern);
	    printf(" e_data[9]: %d\n",e_data[CHAN01]);
	    printf(" pu_flg[9]: %d\n",pu_flg[CHAN01]);
	    printf("e_data[10]: %d\n",e_data[CHAN02]);
	    printf("pu_flg[10]: %d\n",pu_flg[CHAN02]);
	    printf("       Q_E: %lf\n",q_e);
	    fflush(stdout);
	  }
#endif	  
        }
        //else                  // Q_E TEST, Don't show single traces (pattern != 0x0600)
	//  return kTRUE;

	
        //if (time_fract[9] !=0) {                  // NEW 13-05-19
	//  fTime[9]->Fill(time_fract[9]>>2);
	//  fTime[10]->Fill(time_fract[10]>>2);
	//  fTime[8]->Fill(time_fract[6]>>3);	
	//  fTime[9]->Fill(time_fract[6]>>4);
	//}
	
        //printf("\n");
        //fflush(stdout);

        //sleep(1);
//#endif

      }
      //**************************
      // Trace (channel) Packages
      //**************************
      else  
      {
	if(!tr_clr_flag) {
          tr_clr_flag=1;

	  for (i=0;i<FADC_CHAN;i++)          // YYY
	    fTrace[i]->Reset();
	}
	
        //printf("Trace channel found (chan: %d)!\n\n",l_cha_id);
        //fflush(stdout);
        //sleep(1);
        //goto bad_event;

        // channel data size
        l_cha_size = *pl_tmp++;
        //printf("l_cha_size[%d]: %d\n",l_cha_id,l_cha_size);

        // trace header
        l_trace_head = *pl_tmp++;
        //if ( ((l_trace_head & 0xff000000) >> 24) != 0xaa) // here is 0xa!!!
        if ( ((l_trace_head & 0xf0000000) >> 28) != 0xa) 
        {
          //printf ("ERROR>> trace header id is not 0xaa (0x%08x)\n",l_trace_head );
          printf ("ERROR>> trace header id is not 0xaa (0x%08x)\n",l_trace_head );
          goto bad_event; 
        }

        // now trace
        l_trace_size = (l_cha_size/4) - 2;     // in longs/32bit
        //printf("l_trace_size[%d]: %d\n",l_cha_id,l_trace_size);

        for (l_l=0; l_l<l_trace_size; l_l++)   // loop over samples of one trace 
        {
            // disentangle data
            l_dat_fir = *pl_tmp++;
            l_dat_sec = l_dat_fir;

            l_dat_fir =  l_dat_fir & 0x3fff;         // 14bit
            l_dat_sec = (l_dat_sec >> 16) & 0x3fff;

#ifdef GET_E_FROM_TRACE
            //************************************************************************
            // NEW 27-10-18
            // fill energy histogram with value from trapezoid
            // only for chan 6 here! TEST

            if (l_l==50 && l_cha_id==9) {     // l_l counter counts TWO samples!!!
              baseline=l_dat_fir;
              max=l_dat_fir;       // init max
            } 

            if (l_dat_fir>max)     // find max (=E) for trapezoids without PZ
              max=l_dat_fir;

            if (l_l==420 && l_cha_id==9)
              fHisto[l_cha_id]->Fill(l_dat_fir-baseline); // take E value at fixed pos for trapezoids with PZ
	      //fHisto[l_cha_id]->Fill(max-baseline);         // fill max in histogram 

            //*************************************************************************
#endif

            fTrace[l_cha_id]->SetBinContent (l_l*2  ,l_dat_fir);
            fTrace[l_cha_id]->SetBinContent (l_l*2+1,l_dat_sec);

            //fTrace[l_cha_id]->SetBinContent (l_l*2  ,(signed short) (4*l_dat_fir));
            //fTrace[l_cha_id]->SetBinContent (l_l*2+1,(signed short) (4*l_dat_sec));
        }
        l_trace_size = l_trace_size * 2;    

        // trace trailer
        l_trace_trail = *pl_tmp++;
        //if ( ((l_trace_trail & 0xff000000) >> 24) != 0xbb)  // here is 0xb!!!
        if ( ((l_trace_trail & 0xf0000000) >> 28) != 0xb)
        {
          //printf ("ERROR>> trace trailer id is not 0xbb (0x%08x)\n",l_trace_trail);
          printf ("ERROR>> trace trailer id is not 0xb (0x%08x)\n",l_trace_trail);
          printf ("SFP: %d, FEB: %d, CHA: %d \n", l_sfp_id, l_feb_id, l_cha_id); 
          goto bad_event; 
        }
      } // end trace data
    }
    else
    {
      printf ("ERROR>> data word neither channel header nor padding word \n");
    }      

  } // end while


bad_event:

#ifdef EXAMPLE_CODE
  // now we fill histograms from the arrays
  for(i = 0; i<8; i++)
    {
      fCr1Ch[i]->Fill(fCrate1[i]);
      fCr2Ch[i]->Fill(fCrate2[i]);
    }
  value1=fCrate1[0];
  value2=fCrate1[1];
  fHis1->Fill(value1); //fill histograms without gate
  fHis2->Fill(value2);
  if(fconHis1->Test(value1))fHis1gate->Fill(value1); //fill histograms with gate
  if(fconHis2->Test(value2))fHis2gate->Fill(value2);
  // fill Cr1Ch1x2 for three polygons:
  if(fPolyCon->Test(value1,value2))        fCr1Ch1x2->Fill(value1,value2);
  if(((*fConArr)[0])->Test(value1,value2)) fCr1Ch1x2->Fill(value1,value2);
  if(((*fConArr)[1])->Test(value1,value2)) fCr1Ch1x2->Fill(value1,value2);
#endif
#endif
  return kTRUE;
}

//----------------------------END OF GO4 SOURCE FILE ---------------------
