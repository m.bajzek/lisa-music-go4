//---------------------------------------------------------------
//        Go4 Release Package v3.02-0 (build 30200) 
//                      21-July-2006
//---------------------------------------------------------------
//	 The GSI Online Offline Object Oriented (Go4) Project
//	 Experiment Data Processing at EE department, GSI
//---------------------------------------------------------------
//
//Copyright (C) 2000- Gesellschaft f. Schwerionenforschung, GSI
//                    Planckstr. 1, 64291 Darmstadt, Germany
//Contact:            http://go4.gsi.de
//----------------------------------------------------------------
//This software can be used under the license agreements as stated
//in Go4License.txt file which is part of the distribution.
//----------------------------------------------------------------
#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ class TFeb4Param+;
#pragma link C++ class TFeb4Proc+;
#pragma link C++ class TFeb4Event+; 
// MB added 27.02.2024 -----^^^^^

#endif




//----------------------------END OF GO4 SOURCE FILE ---------------------
