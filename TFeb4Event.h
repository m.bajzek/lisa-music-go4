#pragma once

#include "TGo4EventElement.h"
#include "music/music_event_decl.hh"

class TFeb4Event : public TGo4EventElement {
public:
	TFeb4Event();
	TFeb4Event(const char* name);
	virtual ~TFeb4Event();

	virtual void Clear(Option_t* t="");
	virtual Int_t Init();

	MUSIC_EVENT_DECL
	ClassDef(TFeb4Event, 1)
};
