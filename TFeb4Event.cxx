#include "TFeb4Event.h"
#include "Riostream.h"
#include "Go4EventServer/TGo4FileSource.h"

#include "music/music_event_impl.hh"

TFeb4Event::TFeb4Event() : TGo4EventElement("Feb4Event") {}
TFeb4Event::TFeb4Event(const char* name) : TGo4EventElement(name) {}

TFeb4Event::~TFeb4Event() {}

Int_t TFeb4Event::Init() {
	Int_t rev = 0;
	Clear();
	return rev;
}

void TFeb4Event::Clear(Option_t* t) {
	MUSIC_EVENT_CLEAR
}

ClassImp(TFeb4Event)
