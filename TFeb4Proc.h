//---------------------------------------------------------------
//        Go4 Release Package v3.02-0 (build 30200) 
//                      21-July-2006
//---------------------------------------------------------------
//	 The GSI Online Offline Object Oriented (Go4) Project
//	 Experiment Data Processing at EE department, GSI
//---------------------------------------------------------------
//
//Copyright (C) 2000- Gesellschaft f. Schwerionenforschung, GSI
//                    Planckstr. 1, 64291 Darmstadt, Germany
//Contact:            http://go4.gsi.de
//----------------------------------------------------------------
//This software can be used under the license agreements as stated
//in Go4License.txt file which is part of the distribution.
//----------------------------------------------------------------
#ifndef TUNPACKPROCESSOR_H
#define TUNPACKPROCESSOR_H

#include "TGo4EventProcessor.h"
#include "music/music_proc_decl.hh"
TYPEDEFS(TFeb4Event, TFeb4Proc)

class TFeb4Param;

class TFeb4Proc : public TGo4EventProcessor {
   public:
      TFeb4Proc() ;
      TFeb4Proc(const char* name);
      virtual ~TFeb4Proc() ;

      Bool_t BuildEvent(TGo4EventElement* target); // event processing function
	  MUSIC_PROC_DECL
 private:
      TH1I          *fTrace[16];
      TH1I          *fHisto[16];
      TH1I          *fTime[16];
      TH1I          *fCr1Ch[8];
      TH1I          *fCr2Ch[8];
      TH2I          *fCr1Ch9x10;
      TH1I          *fHis1;
      TH1I          *fHis1gate;
      TH1I          *fHis2;
      TH1I          *fHis2gate;
      TGo4WinCond   *fconHis1;
      TGo4WinCond   *fconHis2;
      TGo4PolyCond  *fPolyCon;
      TGo4CondArray *fConArr;
      TGo4MbsEvent  *fInput;
      TFeb4Param      *fParam;
      TGo4Picture   *fPicture;
      TGo4Picture   *fcondSet;
      Float_t        fCrate1[8];
      Float_t        fCrate2[8];

   ClassDef(TFeb4Proc,1)
};
#endif //TUNPACKPROCESSOR_H


//----------------------------END OF GO4 SOURCE FILE ---------------------
